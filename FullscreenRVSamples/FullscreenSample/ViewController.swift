//
//  ViewController.swift
//  FullscreenRVSamples
//
//  Created by Mohamed Matloub on 5/11/21.
//

import UIKit
import AATKit

enum FullScreenAdState {
	case idle, loading, loaded, failed
}

class ViewController: UIViewController {
	@IBOutlet weak var loadAdButton: UIButton!

    var interstitialPlacement: AATFullscreenPlacement?

	var fullScreenAdState: FullScreenAdState = .idle {
		didSet {
			switch fullScreenAdState {
			case .idle:
				loadAdButton.setTitle("Start loading Fullscreen Ads", for: .normal)
			case .loading:
				loadAdButton.setTitle("Loading", for: .normal)
			case .loaded:
				loadAdButton.setTitle("Show Fullscreen Ads", for: .normal)
            case .failed:
                loadAdButton.setTitle("Failed to load ad, click to try again", for: .normal)
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		fullScreenAdState = .idle
        interstitialPlacement = AATSDK.createFullscreenPlacement(name: "placement")
        interstitialPlacement?.delegate = self
        interstitialPlacement?.statisticsDelegate = self
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		// [ADS] - Refresh Ads for placement
		// [ADS] you may set keyword targeting here for placement

        // [ADS] set the current viewController to AATKit
        AATSDK.controllerViewDidAppear(controller: self)
        
        // [ADS] Start auto reloading
        interstitialPlacement?.startAutoReload()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		// [ADS] - stop auto reloading for placement "ProgramPage"
        AATSDK.controllerViewWillDisappear()
	}

	@IBAction func loadAdTapped(_ sender: Any){
		guard let interstitialPlacement = interstitialPlacement else {
			return
		}
		switch fullScreenAdState {
		case .idle, .failed:
            interstitialPlacement.startAutoReload()
            self.fullScreenAdState = .loading
		case .loading:
			print("Ad is loading")
		case .loaded:
			// [ADS] displaying loaded FullScreen Ad
            interstitialPlacement.show()

			self.fullScreenAdState = .loading
		}
	}
}
extension ViewController: AATFullscreenPlacementDelegate {
    func aatPauseForAd(placement: AATKit.AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatHaveAd(placement: AATPlacement) {
        fullScreenAdState = .loaded
    }
    
    func aatNoAd(placement: AATPlacement) {
        fullScreenAdState = .failed
    }
}

extension ViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }

    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
}
