//
//  LaunchViewController.swift
//  StartInterstitial
//
//  Created by Mohamed Matloub on 5/12/21.
//

import UIKit
import AATKit

class LaunchViewController: UIViewController {
	@IBOutlet weak var timerLabel: UILabel!

	let maxWaitingTime: TimeInterval = 10
	var timer: Timer!
	var startTime: Date!

    var interstitialPlacement: AATFullscreenPlacement?

	override func viewDidLoad() {
		super.viewDidLoad()
        
        interstitialPlacement = AATSDK.createFullscreenPlacement(name: "placement")
        interstitialPlacement?.delegate = self
        interstitialPlacement?.statisticsDelegate = self

		let now = Date()
		startTime = now

		timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
	}

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // [ADS] - Refresh Ads for placement
        // [ADS] you may set keyword targeting here for placement

        // [ADS] set the current viewController to AATKit
        AATSDK.controllerViewDidAppear(controller: self)
        
        // [ADS] Start auto reloading
        interstitialPlacement?.startAutoReload()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // [ADS] - stop auto reloading for placement "ProgramPage"
        AATSDK.controllerViewWillDisappear()
    }

	@objc
	func tick() {
		let timeDiff = Date().timeIntervalSince(self.startTime)
		if timeDiff >= maxWaitingTime {
			self.routeToHome()
		}
		self.timerLabel.text = "Opening App in \(Int(maxWaitingTime - timeDiff))s"
	}

	func routeToHome() {
		timer.invalidate()
		let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
		UIApplication.shared.keyWindow?.rootViewController = viewController
	}
}

extension LaunchViewController: AATFullscreenPlacementDelegate {
    func aatPauseForAd(placement: AATKit.AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        routeToHome()
    }
    
    func aatHaveAd(placement: AATPlacement) {
        let timeDiff = Date().timeIntervalSince(self.startTime)
        guard let interstitialPlacement = interstitialPlacement,
              timeDiff < maxWaitingTime else {
            return
        }

        timer.invalidate()
        interstitialPlacement.show()
    }
    
    func aatNoAd(placement: AATPlacement) {
        print("No ads available")
    }
}

extension LaunchViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }

    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
}
