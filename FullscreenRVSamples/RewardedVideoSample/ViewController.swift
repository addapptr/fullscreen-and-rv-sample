//
//  ViewController.swift
//  RewardedVideoSample
//
//  Created by Mohamed Matloub on 5/11/21.
//

import UIKit
import AATKit

enum rewardedVideoAdState {
	case idle, loading, loaded, failed
}

class ViewController: UIViewController {
	@IBOutlet weak var loadAdButton: UIButton!

	var rewardedVideoPlacement: AATRewardedVideoPlacement?

	var rewardedVideoAdState: rewardedVideoAdState = .idle {
		didSet {
			switch rewardedVideoAdState {
			case .idle:
				loadAdButton.setTitle("Start loading rewarded Video Ads", for: .normal)
			case .loading:
				loadAdButton.setTitle("Loading", for: .normal)
			case .loaded:
				loadAdButton.setTitle("Show rewarded Video Ad", for: .normal)
            case .failed:
                loadAdButton.setTitle("Failed to load ad, click to try again", for: .normal)
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
        rewardedVideoPlacement = AATSDK.createRewardedVideoPlacement(name: "RewardedVideoPlacement1")
        rewardedVideoPlacement?.delegate = self
        rewardedVideoPlacement?.statisticsDelegate = self

        rewardedVideoAdState = .idle
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		// [ADS] - Refresh Ads for placement
		// [ADS] you may set keyword targeting here for placement

		// [ADS] set the current viewController to AATKit
        AATSDK.controllerViewDidAppear(controller: self)

        rewardedVideoPlacement?.startAutoReload()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		// [ADS] - stop auto reloading for placement "ProgramPage"
        AATSDK.controllerViewWillDisappear()
	}

	@IBAction func loadAdTapped(_ sender: Any){
		guard let placement = rewardedVideoPlacement else {
			return
		}
		switch rewardedVideoAdState {
        case .idle, .failed:
            placement.startAutoReload()
            self.rewardedVideoAdState = .loading
		case .loading:
			print("Ad is loading")
		case .loaded:
			// [ADS] displaying loaded RewardedVideo Ad
            placement.show()
			self.rewardedVideoAdState = .loading
		}
	}
}

extension ViewController: AATRewardedVideoPlacementDelegate {
    func aatUserEarnedIncentive(placement: AATPlacement, aatReward: AATReward) {
        let alert = UIAlertController(title: "Rewarded Video Sample", message: "User earned reward", preferredStyle: .alert)
        alert.addAction(.init(title: "ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func aatPauseForAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatHaveAd(placement: AATPlacement) {
        rewardedVideoAdState = .loaded
    }
    
    func aatNoAd(placement: AATPlacement) {
        rewardedVideoAdState = .failed
    }
}

extension ViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }

    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }

    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
}
